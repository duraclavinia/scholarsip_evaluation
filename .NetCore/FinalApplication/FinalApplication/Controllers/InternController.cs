﻿using FinalApplication.Models;
using FinalApplication.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalApplication.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class InternController : ControllerBase
    {

        IInternCollectionService _internCollectionService;
        public InternController(IInternCollectionService internCollectionService)
        {
            _internCollectionService = internCollectionService;
        }

        /// <summary>
        ///     Return all the interns
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetInterns()
        {
            return Ok(await _internCollectionService.GetAllInterns());
        }


        /// <summary>
        ///   Add a new intern to the list
        /// </summary>
        /// <param name="intern"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddIntern([FromBody] Intern intern)
        {
            if (intern == null)
                return BadRequest("Intern is null");
            await _internCollectionService.Create(intern);
            return Ok(await _internCollectionService.GetAllInterns());
        }


        /// <summary>
        ///     Delete an intern by a specified id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInternById(Guid id)
        {
            if (!await _internCollectionService.Delete(id))
            {
                return NotFound("Intern not found");
            }
            return Ok("Intern was deleted");
        }


        /// <summary>
        ///     Update an intern by it's id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="intern"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateIntern(Guid id, [FromBody] Intern intern)
        {

            if (intern == null)
                return BadRequest("Intern is null");
            if (!await _internCollectionService.Update(id, intern))
            {
                return NotFound("Intern not found");
            }
            return Ok(await _internCollectionService.GetAllInterns());

        }

        /// <summary>
        ///     Return an intern by a specified id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            if (id == null)
            {
                return BadRequest("The id is null");
            }
            var intern = await _internCollectionService.GetInternById(id);
            if (intern == null)
            {
                return NotFound("Intern's id not found");
            }
            return Ok(intern);
        }
    }
}
