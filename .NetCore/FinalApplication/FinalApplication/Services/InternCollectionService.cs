﻿using FinalApplication.Models;
using FinalApplication.Settings;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalApplication.Services
{
    public class InternCollectionService : IInternCollectionService
    {
        public InternCollectionService() { }

        private readonly IMongoCollection<Intern> _interns;

        public InternCollectionService(IMongoDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _interns = database.GetCollection<Intern>(settings.InternCollectionName);
        }

        public async Task<List<Intern>> GetAllInterns()
        {
            var result = await _interns.FindAsync(intern => true);
            var internsList = result.ToList();
            return internsList;
        }

        public async Task<Intern> GetInternById(Guid id)
        {
            return (await _interns.FindAsync(intern => intern.Id == id)).FirstOrDefault();
        }

        public async Task<bool> Create(Intern intern)
        {
            await _interns.InsertOneAsync(intern);
            return true;
        }

        public async Task<bool> Update(Guid id, Intern intern)
        {
            intern.Id = id;
            var result = await _interns.ReplaceOneAsync(employee => employee.Id == id, intern);
            if (!result.IsAcknowledged && result.ModifiedCount == 0)
            {
                await _interns.InsertOneAsync(intern);
                return false;
            }

            return true;
        }

        public async Task<bool> Delete(Guid id)
        {
            var result = await _interns.DeleteOneAsync(intern => intern.Id == id);
            if (!result.IsAcknowledged && result.DeletedCount == 0)
            {
                return false;
            }
            return true;
        }
    }
    
}
