import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddInternComponent } from './intern/add-intern/add-intern.component';
import { InternsComponent } from './intern/interns/interns.component';

const routes: Routes = [
  { path: '', component: InternsComponent },
  { path: "add-intern", component: AddInternComponent },
  { path: '**', redirectTo: '' }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
