export interface Intern {
    id: string;
    firstName: string;
    lastName: string;
    age: number;
    dateOfBirth: string;
    gender: string;
    city: string;
    phoneNumber: string;
}
