import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-button',
  templateUrl: './add-button.component.html',
  styleUrls: ['./add-button.component.scss']
})
export class AddButtonComponent implements OnInit {
  constructor(private _router: Router) { }

  ngOnInit(): void {
  }

  addIntern(): void {

    this._router.navigateByUrl('/addIntern');
  }


}
