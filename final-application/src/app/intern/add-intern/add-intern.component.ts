import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InternServiceService } from 'src/app/services/intern-service.service';

@Component({
  selector: 'app-add-intern',
  templateUrl: './add-intern.component.html',
  styleUrls: ['./add-intern.component.scss']
})
export class AddInternComponent implements OnInit {

  // firstName:string ="";
  // lastName:string = "";
  // age:number= 0;
  // dateOfBirth:string = "";
  // gender:string ="";
  // city:string ="";
  // phoneNumber:string ="";
  firstNameError: boolean = false;
  lastNameError: boolean = false;
  cityError: boolean = false;

  formGroup = new FormGroup({
    firstName: new FormControl("", [Validators.required, Validators.minLength(3)]),
    lastName: new FormControl("", Validators.required),
    age: new FormControl("",Validators.required),
    dateOfBirth: new FormControl("",Validators.required),
    gender: new FormControl("", Validators.required),
    city: new FormControl("", Validators.required),
    phoneNumber: new FormControl("", Validators.required),
  })

  constructor(private _router: Router, private _service: InternServiceService) { }

  ngOnInit(): void {
    // this._activatedRoute.queryParams.subscribe(params => {
    //   this.firstName = firstName;
    //   this.lastName=lastName;
    //   this.age=age;
    //   this.dateOfBirth=dateOfBirth;
    //   this.city = this.city;
  }
  ngAfterViewInit(): void {
    this.formGroup.controls['firstName'].valueChanges.subscribe((firstName) => {
      if (firstName.length < 3) {
        this.firstNameError = true;
      } else {
        this.firstNameError = false;
      }
    })


    this.formGroup.controls['lastName'].valueChanges.subscribe((lastName) => {
      if (lastName.length < 3) {
        this.lastNameError = true;
      } else {
        this.lastNameError = false;
      }
    })

    this.formGroup.controls['city'].valueChanges.subscribe((city) => {
      if (city.length < 3) {
        this.cityError = true;
      } else {
        this.cityError = false;
      }
    })
  }

  addIntern(): void {
    this._service.addIntern(this.formGroup.value).subscribe(() => this._router.navigateByUrl(''));
  }

}
