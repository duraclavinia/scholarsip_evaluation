import { Component, EventEmitter, Input, OnInit, Output, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Intern } from 'src/app/interfaces/intern';
import { InternServiceService } from 'src/app/services/intern-service.service';

@Component({
  selector: 'app-edit-intern',
  templateUrl: './edit-intern.component.html',
  styleUrls: ['./edit-intern.component.scss']
})
export class EditInternComponent implements OnInit {

  internIdExists: boolean = false;

  ngOnInit(): void {
  }


  @Input() set internId(internId: string) {
    if (internId !== '') {
      this._service.getIntern(internId).subscribe((intern: Intern) => {
        this.formGroup.patchValue(intern)
      })
      this.internIdExists = true;
    } else {
      this.internIdExists = false;
    }
  }

  @Output() userEdited: EventEmitter<void> = new EventEmitter<void>();

  formGroup = new FormGroup({
    id: new FormControl(""),
    firstName: new FormControl("", [Validators.required, Validators.minLength(3)]),
    lastName: new FormControl("", Validators.required),
    age: new FormControl("", Validators.required),
    dateOfBirth: new FormControl("", Validators.required),
    gender: new FormControl("", Validators.required),
    city: new FormControl("", Validators.required),
    phoneNumber: new FormControl("", Validators.required),
  });

  constructor(private _activatedRoute: ActivatedRoute, private _service: InternServiceService) { }

  editIntern() {
    this._service.editIntern(this.formGroup.value).subscribe((result) => {
      this.userEdited.next();
      this.internIdExists = false;
    })
  }

}
