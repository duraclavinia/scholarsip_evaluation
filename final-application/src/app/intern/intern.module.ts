import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { InternsComponent } from './interns/interns.component';
import { AddInternComponent } from './add-intern/add-intern.component';
import { EditInternComponent } from './edit-intern/edit-intern.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { MatNativeDateModule } from '@angular/material/core/datetime';
import { MatCommonModule } from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import { MatFormFieldModule, MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import { MatCard, MatCardModule } from '@angular/material/card';
import { MatRadioButton, MatRadioModule } from '@angular/material/radio';
import { HomeComponent } from './home/home.component';
import { AddButtonComponent } from './add-button/add-button.component';
@NgModule({
  declarations: [
    InternsComponent,
    AddInternComponent,
    EditInternComponent,
    HomeComponent,
    AddButtonComponent
  ],
  imports: [
    CommonModule,
     FormsModule,
     ReactiveFormsModule,
     MatButtonModule,
     MatIconModule,
     MatNativeDateModule,
     MatCommonModule,
     HttpClientModule,
     MatInputModule,
     MatFormFieldModule,
     MatCardModule,
     MatRadioModule,
     MatRadioButton,
     MatCard
  ],
  exports: [
    InternsComponent,
    EditInternComponent,
    AddInternComponent,

  ],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } }
  ]
})
export default class InternModule { }
