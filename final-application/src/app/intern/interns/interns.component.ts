import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { Intern } from 'src/app/interfaces/intern';
import { InternServiceService } from 'src/app/services/intern-service.service';

@Component({
  selector: 'app-interns',
  templateUrl: './interns.component.html',
  styleUrls: ['./interns.component.scss']
})
export class InternsComponent implements OnInit {

  interns: Intern[] = [];
  internId: string = "";
  sortButton: string = "";

  constructor(private _router: Router, private _internService: InternServiceService) { }

  ngOnInit(): void {
    this.getInterns();
  }

  getInterns()
  {
    this._internService.getInterns().subscribe((result) => {
      this.interns = result;
    });
  }

  deleteIntern(id: string) {
    this._internService.deleteIntern(id).subscribe(() => this.getInterns());
    this.internId = "";
  }
  editIntern(id: string) {
    this.internId = id;
  }

  sortAscending(): void {
    this.sortButton = 'ascending';
    this.interns = this.interns.sort((a, b) => (a.firstName > b.firstName) ? 1 : ((b.firstName > a.firstName) ? - 1 : 0));
  }

  sortDescending(): void {
    this.sortButton = 'descending';
    this.interns = this.interns.sort((b, a) => (a.firstName > b.firstName) ? 1 : ((b.firstName > a.firstName) ? - 1 : 0));
  }

}
