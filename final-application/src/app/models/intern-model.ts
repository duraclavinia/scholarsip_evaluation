import { Intern } from "../interfaces/intern";

export class InternModel implements Intern{
    id: string;
    firstName: string;
    lastName: string;
    age: number;
    dateOfBirth: string;
    gender: string;
    city: string;
    phoneNumber: string;

    constructor(id: string, firstName:string,lastName:string,age:number,dateOfBirth:string,gender:string,city:string,phoneNumber:string){
        this.id=id;
        this.firstName=firstName;
        this.lastName=lastName;
        this.age=age;
        this.dateOfBirth=dateOfBirth;
        this.gender=gender;
        this.city=city;
        this.phoneNumber=phoneNumber;
    }

}
