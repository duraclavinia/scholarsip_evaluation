import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Intern } from '../interfaces/intern';
import { InternModel } from '../models/intern-model';

@Injectable({
  providedIn: 'root'
})
export class InternServiceService {

  readonly baseUrl = 'https://localhost:44371/';

  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  
  constructor(private httpClient: HttpClient) { }

  getInterns():Observable<Intern[]>{
    return this.httpClient.get<Intern[]>(this.baseUrl + '/interns', this.httpOptions);
  }
  getIntern(id: string): Observable<Intern> {
    return this.httpClient.get<Intern>(this.baseUrl + '/intern/' + id);
  }

  addIntern(
    intern: InternModel
  ): Observable<Intern[]> {
    return this.httpClient.post<Intern[]>(this.baseUrl + '/intern', intern);
  }

  deleteIntern(id: string) {
    return this.httpClient.delete(this.baseUrl + '/intern/' + id);
  }

  editIntern(
    intern: Intern
  ): Observable<Intern> {
    return this.httpClient.put<Intern>(this.baseUrl + '/intern/' + intern.id, intern);
  }

  getSortedInterns(order: string) {
    return this.httpClient
      .get<Intern[]>(this.baseUrl + '/intern', this.httpOptions)
      .pipe(map((interns) => {
        if (order=="ascending") {
          interns = interns.sort((a,b) => a.firstName.localeCompare(b.firstName));
        }
        if (order=="descending") {
          interns = interns.sort((a,b) => a.firstName.localeCompare(b.firstName)).reverse();
        }

        return interns;
      }));
  }
}
